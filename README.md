# PROYECTO 4: VISUALIZADOR DE PROTEÍNAS DUMBO

_Este proyecto consiste en la creación de un visualizador de archivos PBD (Protein Data Bank) que otorga la información del archivo seleccionado además de una imagen de la proteína seleccionada. Existen cuatro opciones para visualizar la información de la proteína: Atom, Hetatm, Anisou y Others. El usuario puede guardar un archivo de texto de lo que ha visualizado con información de cada una de las cuatro opciones._

### Pre-requisitos:

- Sistema operativo Linux
- Python3
- Glade Interfax Designer
- PyMOL
- gi
- Pandas
- BioPandas

### Instalación:

1. Descargue todos los archivos del siguiente repositorio: https://gitlab.com/proy2/proyecto4.git
2. Abra su terminal y dirijase al lugar donde están ubicados los archivos que acaba de descargar
3. Escriba python3 ventana.py en su terminal y presione ENTER
4. Espere algunos segundos para que se ejecute el programa 

### Instrucciones:

1. Descargue un archivo .pdb de la siguiente página web: http://www.rcsb.org
2. Haga click en el botón abrir y luego cierre la ventana emergente
3. presione nueva mente en el boton abrir y seleccione el archivo .pdb que desea visualizar (hacer doble click en el archivo escogido)
4. Haga click en el botón elegir para seleccionar el tipo de información que quiere visualizar, existen 4 opciones: Atom, Hetatm, Anisou y Others
5. Haga click en el botón guardar para guardar la información de la opción seleccionada en su computador
6. El botón información sirve para mencionar a las autoras del programa y su versión

### PEP8 en el programa:

La utilización de la PEP8 a lo largo de todo el código fue fundamental para el correcto funcionamiento del programa. Ejemplo:

```
    def conseguir_nombre(self, btn=None):

        modelo = self.combobox.get_model()
        indice = self.combobox.get_active_iter()
        nombre = modelo[indice][0]
        #imagen = gen_image("5fhc.pdb","5fhc.pdb") 
        #self.image.set_from_file("5fhc.pdb.png")
        #self.add(self.image) 
        #resumen =imprimir_resumen() 
        #self.label.set_text(f"{resumen} ") 

        if nombre == "Atom":
            atomo =atom()
            ppdb = PandasPdb() 
            ppdb.read_pdb('5fhc.pdb_ATOM.pdb')
            self.label.set_text( ppdb.pdb_text[:3000])
 
        elif nombre == "Hetatm":
            hetatm_ = hetatm()
            ppdb = PandasPdb()
            ppdb.read_pdb('5fhc.pdb_HETATM.pdb')  
            self.label.set_text( ppdb.pdb_text[:1000])   
    
        elif nombre == "Anisou":
            anisuo = anisou()
            ppdb = PandasPdb()
            ppdb.read_pdb('5fhc.pdb_ANISOU.pdb')
            self.label.set_text( ppdb.pdb_text[:3000])
               
        elif nombre == "Others":
            other = others()
            ppdb = PandasPdb()
            ppdb.read_pdb('5fhc.pdb_OTHERS.pdb') 
            self.label.set_text( ppdb.pdb_text[:3000])
```
Esta función se encuentra dentro de la función principal y es por eso que tiene 4 espacios en su comienzo. Un claro ejemplo de la utilización de la PEP8 en el código, son los 4 espacios presentes después de definir una función, un ciclo o una condicional. Cada vez que se requiera definir otra estructura dentro de una estructura ya creada, se deben usar 4 espacios como lo muestra el extracto. Luego de definir la estructura a utilizar se deben escribir : y los operadores de comparación como == o booleans como "or" deben tener un espacio en blanco a cada lado.

### Creado con:

- GNU/Linux
- Python3 
- VIM
- VSCodium
- Glade
- GTK
- PDB
- Pandas
- BioPandas
- PyMOL
- GitLab

### Links de archivos PDB utilizados:

https://www.rcsb.org/structure/5FHC

https://www.rcsb.org/structure/6QD7 

### Autoras:

Paz Echeverría - [pazjecheverriaf](https://gitlab.com/pazjecheverriaf)

Patricia Fuentes - [PatriciaFuentesG](https://gitlab.com/PatriciaFuentesG)

Magdalena Lolas - [mlolas](https://gitlab.com/mlolas)
