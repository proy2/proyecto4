import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class DlgFileChooser():

    def __init__(self, option=None):
        builder = Gtk.Builder()
        builder.add_from_file("ventana_pdb.ui")

        self.dialogo = builder.get_object("choose")

        if option == "open":
            self.dialogo.add_buttons(Gtk.STOCK_CANCEL,
                                     Gtk.ResponseType.CANCEL,
                                     Gtk.STOCK_OPEN,
                                     Gtk.ResponseType.OK)

            
        self.dialogo.show_all()
