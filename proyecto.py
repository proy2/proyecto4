import __main__
# Pymol : quiet and no GUI
__main__ . pymol_argv=[ "pymol","-qc"]
import pymol
pymol.finish_launching()
from biopandas.pdb import PandasPdb
def gen_image ( name,filename,path="./" ):
    aux = name.split(".")
    pdb_name = aux[0]
    png_file = "".join([path])
    pdb_file = filename
    pymol.cmd.load (filename,name )
    pymol.cmd.disable("all")
    pymol.cmd.enable (name)
    pymol.cmd.hide ("all")
    pymol.cmd.show ("cartoon")
    pymol.cmd.set ("ray_opaque_background", 0 )
    pymol.cmd.pretty(name)
    pymol.cmd.png(filename)
    pymol.cmd.ray()

