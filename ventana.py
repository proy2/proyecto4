import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from biopandas.pdb import PandasPdb 
from desarolladoras import GtkDialog
from os import remove
from escoger import DlgFileChooser 
from proyecto import gen_image
class MainWindow():
     def __init__(self):
         self.builder = Gtk.Builder()
         self.builder.add_from_file("ventana_pdb.ui")
         self.window = self.builder.get_object("ventana")
         self.window.connect("destroy", Gtk.main_quit)
         self.window.set_title("Visualizador de proteina")
         self.window.resize(800, 600)

         self.lista_variables = Gtk.ListStore(str)
         cell = Gtk.CellRendererText()
         self.combobox = self.builder.get_object("elegir")
         self.combobox.connect("changed", self.conseguir_nombre)

         self.boton_creadoras = self.builder.get_object("informacion")
         self.boton_creadoras.connect("clicked", self.abrir_dialogo)
        
         self.boton_open = self.builder.get_object("open")
         self.boton_open.connect("clicked", self.open_dialogo)
         
         self.boton_guardar = self.builder.get_object("almacenar")
         self.boton_guardar.connect("clicked", self.guardar_dialogo)
         self.tree = self.builder.get_object("tree")
          
         self.boton_open.connect("clicked", self.open_dialogo, "open")
    
  
         self.label = self.builder.get_object("proteina") 
         self.labe = self.builder.get_object("nombre")
         self.resumen = self.builder.get_object("resumen")  
         self.image = self.builder.get_object("imagen_proteina")
    
         self.tree = self.builder.get_object("informacion")
         self.combobox.pack_start(cell, True)
         self.combobox.add_attribute(cell, "text", 0)
 
         Nombres = ["Atom",
                    "Hetatm",
                    "Anisou",
                    "Others"]
         for nombre in Nombres:
             self.lista_variables.append([nombre])

         self.combobox.set_model(model=self.lista_variables)
         self.window.show_all()
                  
     def abrir_dialogo(self, btn=None):
         dlg = GtkDialog()
         response = dlg.dialogo.run()
    
     def open_dialogo(self, btn=None, opt=None):
         filechooser = DlgFileChooser(option=opt)
         dialogo = filechooser.dialogo
         response = dialogo.run()
         if response == Gtk.ResponseType.OK:
             self.variable = dialogo.get_filename()
         dialogo.destroy()

     def guardar_dialogo(self, btn = None): 
         name = self.variable 
         nombre = self.nombres
         if nombre == "Atom":
             ppdb = PandasPdb()
             ppdb.read_pdb(f"{name}")
             ppdb.to_pdb(path =f"{name}_ATOM.pdb",
                         records =["ATOM"],
                         gz = False,
                         append_newline = True)
         if nombre == "Hetatm":    
             ppdb = PandasPdb()
             ppdb.read_pdb(f'{name}_HETATM.pdb')
             ppdb.to_pdb(path =f"{name}_HETATM.pdb",
                         records =["HETATM"],
                         gz = False,
                         append_newline = True)
         if nombre == "Anisou":
             ppdb = PandasPdb()
             ppdb.read_pdb(f"{name}")
             ppdb.to_pdb(path =f"{name}_ANISOU.pdb",
                         records =["ANISOU"],
                         gz = False,
                         append_newline = True)
         if nombre == "Others":
             ppdb = PandasPdb()
             ppdb.read_pdb(f'{name}')
             ppdb.to_pdb(path =f"{name}_OTHERS.pdb",
                         records =["OTHERS"],
                         gz = False,
                         append_newline = True)
    
     def conseguir_nombre(self, name):
         name = self.variable
         self.guardar = 1
         largo = len(name)
         num = largo - 8
         self.labe.set_text( f"Nombre de la proteina:{name[num:largo]}")
         imagen = gen_image("5fhc.pdb" ,"5fhc.pdb") 
         image = gen_image("6qd7.pdb","6qd7.pdb")
         self.image.set_from_file(f"{name}.png")
         if name[num:largo]=="5fhc.pdb":
             self.resumen.set_text("El virus del Ébola causa fiebre hemorrágica con una alta, tasa de letalidad para la cual no existia una terapia aprobada. ")
         if name[num:largo]=="6qd7.pdb":
             self.resumen.set_text("El virus de la estomatitis vesicular recombinante-El virus del Ébola de Zaire  es el candidato a vacuna contra el virus del Ébola más avanzado")
         modelo = self.combobox.get_model()
         indice = self.combobox.get_active_iter()
         if indice == None:
             indice  = 0
         nombre = modelo[indice][0]
         self.nombres = nombre
         
         if nombre == "Atom":
             self.image.set_from_file(f"{name}.png")
             ppdb = PandasPdb()
             ppdb.read_pdb(f"{name}")
             ppdb.to_pdb(path =f"{name}_ATOM.pdb",
                         records =["ATOM"],
                         gz = False,
                         append_newline = True)
             ppdb.read_pdb(f'{name}_ATOM.pdb')
             self.label.set_text( ppdb.pdb_text[:2400])   
             remove(f"{name}_ATOM.pdb")
 
         elif nombre == "Hetatm":
             self.image.set_from_file(f"{name}.png")
             ppdb = PandasPdb()
             ppdb.read_pdb(f'{name}_HETATM.pdb')  
             ppdb.to_pdb(path =f"{name}_HETATM.pdb",
                         records =["HETATM"],
                         gz = False,
                         append_newline = True)
             ppdb.read_pdb(f'{name}_HETATM.pdb')
             self.label.set_text( ppdb.pdb_text[:2400])
             remove(f"{name}_HETATM.pdb")
    
         elif nombre == "Anisou":
             self.image.set_from_file(f"{name}.png")
             ppdb = PandasPdb()
             ppdb.read_pdb(f"{name}")
             ppdb.to_pdb(path =f"{name}_ANISOU.pdb",
                         records =["ANISOU"],
                         gz = False,
                         append_newline = True)
             ppdb.read_pdb(f'{name}_ANISOU.pdb')
             self.label.set_text( ppdb.pdb_text[:2400])
             remove(f"{name}_ANISOU.pdb")
               
         elif nombre == "Others":
             self.image.set_from_file(f"{name}.png")
             ppdb = PandasPdb()
             ppdb.read_pdb(f'{name}') 
             ppdb.to_pdb(path =f"{name}_OTHERS.pdb",
                         records =["OTHERS"],
                         gz = False,
                         append_newline = True)
             ppdb.read_pdb(f'{name}_OTHERS.pdb')
             self.label.set_text( ppdb.pdb_text[:2400])
             remove(f"{name}_OTHERS.pdb")
if __name__ == "__main__":
    MainWindow()
    Gtk.main()
